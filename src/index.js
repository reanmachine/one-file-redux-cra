// React Dependencies
import React from 'react';
import ReactDOM from 'react-dom';

// Redux
import { 
  // Used to combine reducers in a performant way, not required but saves
  // a lot of legwork.
  combineReducers, 
  // Normally only needed inside the index.js where the store is initialized
  createStore 
} from 'redux';

// React-Redux

import { 
  // Needed in any "connected" component to literally "connect" them to the store.
  connect,
  // A provider component to register the store in the component
  // context object and provide it to connect(). Only needed in the root index.js
  // typically.
  Provider
} from 'react-redux';

// CSS Import(s)
import './index.css';

// Constants
// ----------------------------------------
const TEXT_CHANGE_VALUE = "text/changeValue";
const TEXT_CHANGE_COLOR = "text/changeColor";

const HEX_ALPHABET = '01234567890ABCDEF';

// Action Creators
// ----------------------------------------

// Most action creators just create the message
// they're usually pretty simple so they can be passed
// around, saved and replayed if needed.
const changeTextAction = text => ({
  type: TEXT_CHANGE_VALUE,
  payload: {
    text
  }
});

// Sometimes your action creators can do basic logic to
// format the message, this one randomly generates a hex
// color to send in.
const changeColorAction = () => {
  let hex = [];
  for(let i = 0; i < 6; i++) {
    hex.push(HEX_ALPHABET[Math.floor(Math.random() * HEX_ALPHABET.length)]);
  }

  return {
    type: TEXT_CHANGE_COLOR,
    payload: {
      color: hex.join('')
    }
  }
};

// Reducers
// ----------------------------------------

// It's nice to have a default state for each reducer,
// it allows the redux INIT action to actually initialize
// the data in a modular way.
const defaultTextState = {
  value: 'Nothing',
  color: '#000000'
};

// This is the text reducer, a smaller sub-module for the 
// state.text namespace
const textReducer = (state = defaultTextState, action) => {
  switch(action.type) {
    case TEXT_CHANGE_VALUE:
      return {
        ...state,
        value: action.payload.text
      }
    case TEXT_CHANGE_COLOR:
      return {
        ...state,
        color: `#${action.payload.color}`
      }
    default:
      // If we were using immutable.js it wouldn't
      // matter if we just sent back state, but
      // it's important that we send back a different
      // ref here as we want to make sure we dont create
      // side effects accidentally down the line.
      // This is just a shallow copy.
      return {...state};
  }
};

const masterReducer = combineReducers({
  text: textReducer,
  // This is just here to demonstrate how you can combine multiple to make
  // data namespaces
  other: (state = {}, action) => ({...state})
});

// Components
// ----------------------------------------

// Functional Compoment faximilies are allowed
// they're basic view-only components that are derived
// from a function that takes in `props` and outputs
// dom.
const ViewComponent = ({ text, color }) => (
  <div className="text" style={{color: color}}>{text}</div>
);

// Fully realized React components are allowed too
// and mixing the two is fine, they both have purpose
class ControlsComponent extends React.Component {
  constructor(props) {
    super(props);

    // Local state isn't as taboo as people
    // make it out to be, just realize what you're
    // giving up when you use it.
    this.state = { text: '' };
  };

  // This format uses the arrow function's this
  // inferrence to auto-bind it to the object
  // so `this` always means the component instance.
  updateText = (event) => {
    // React events use synthetic event objects
    // event.target is the node that threw it
    this.setState({
      text: event.target.value
    });
  };
  
  render() {
    const {
      changeColor,
      changeText
    } = this.props;

    const {
      text
    } = this.state;

    return (
      <div className="controls">
        <input type="text" onChange={this.updateText} />
        <button onClick={() => changeText(text)}>Update Text</button>
        <button onClick={() => changeColor()}>Random Color</button>
      </div>
    );
  }
}

const AppComponent = ({ color, text, changeColor, changeText }) => (
  <div>
    <h4>Example</h4>
    <ViewComponent color={color} text={text} />
    <h4>Edit</h4>
    <ControlsComponent changeColor={changeColor} changeText={changeText} />
  </div>
);

// Normally the thing we actually export from a component is the connected HOC (higher-order component).
const App = connect(
  // State transformation
  state => ({
    color: state.text.color,
    text: state.text.value
  }),
  // Action Auto-binding
  dispatch => ({
    changeColor: () => dispatch(changeColorAction()),
    changeText: (text) => dispatch(changeTextAction(text))
  })
  // connect() returns a function that accepts the inner component to wrap
  // that's why we have (AppComponent)
)(AppComponent);
// The result is the logic-appended (mixin effectively) performance-optimized
// smart component that wraps AppComponent and provides it's redux-stuff as Props
// which is what is assigned into 'App'

// App Buildup
// ----------------------------------------

var store = createStore(masterReducer);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
